pipeline {
 agent any
 environment {
     APP_VERSION = '1.5.43'
     MAVEN_PROFILE = 'dev'
     CLUSTER_NAME = 'cluster-ze'
     BRANCH_NAME = 'dev-cluster-ze'
     GCP_PROJECT_ID = 'crested-axe-330308'
     DEV_DB_URL = credentials('dev-db-url')
     DEV_DB_USER = credentials('dev-db-user')
     DEV_DB_PASS = credentials('dev-db-pass')
     QA_DB_URL = credentials('qa-db-url')
     QA_DB_USER = credentials('qa-db-user')
     QA_DB_PASS = credentials('qa-db-pass')
 }
  stages {
    stage('Build') {
      steps {
        sh 'mvn compile -DskipTests'
        sh 'mvn clean package -P${MAVEN_PROFILE} -DskipTests -Dmaven.install.skip=true'
      }
    }

    stage('SonarQube') {
      stages {
        stage('SonarQube analysis'){
          steps {
              withSonarQubeEnv('sonarqube'){
                sh "mvn sonar:sonar"
              }
          }
        }
        stage("SonarQube Quality Gate"){
          steps {
              script {
                timeout(time: 5, unit: 'MINUTES') {
                    def qg = waitForQualityGate()
                    if (qg.status != 'OK') {
                      error "Pipeline aborted due to quality gate failure: ${qg.status}"
                    }
                }
              }
          }
        }
      }
    }

    stage('Docker Build') {
      steps {
        sh 'gcloud auth configure-docker'
        sh 'docker build --tag=scrumify-${BRANCH_NAME}:v${APP_VERSION} ./'
      }
    }

    stage('Publish Image') {
      steps {
        sh 'docker image tag scrumify-${BRANCH_NAME}:v${APP_VERSION} eu.gcr.io/${GCP_PROJECT_ID}/scrumify-${BRANCH_NAME}:v${APP_VERSION}'
        sh 'docker push eu.gcr.io/${GCP_PROJECT_ID}/scrumify-${BRANCH_NAME}:v${APP_VERSION}'
      }
    }

    stage('Deployment') {
      stages{
        stage('Connect to Cluster') {
          steps{
            sh 'gcloud container clusters get-credentials ${CLUSTER_NAME} --region europe-north1-a --project ${GCP_PROJECT_ID}'
          }
        }
        stage('Configure Files') {
          steps {
            sh "sed -i 's/##ENVIRONMENT_NAME##/dev-cluster-ze/g' namespace.yaml"

            sh "sed -i 's/##Project##/crested-axe-330308/g' kubernetes/deployment.yaml"
            sh "sed -i 's/##Revision##/${APP_VERSION}/g' kubernetes/deployment.yaml"
            sh "sed -i 's/##Branch##/dev-cluster-ze/g' kubernetes/deployment.yaml"

            sh "sed -i 's/##Revision##/${APP_VERSION}/g' kubernetes/service.yaml"
            sh "sed -i 's/##Branch##/dev-cluster-ze/g' kubernetes/service.yaml"

            sh "sed -i 's/##Revision##/${APP_VERSION}/g' kubernetes/istio-config.yaml"
            sh "sed -i 's/##Branch##/dev-cluster-ze/g' kubernetes/istio-config.yaml"
          }
        }
        stage('Apply Namespace Condition') {
          steps {
            sh 'kubectl apply -f namespace.yaml'
          }
        }
        stage('Apply Deployment') {
          steps {
            sh 'kubectl config set-context --current --namespace=dev-cluster-ze'
            sh 'kubectl delete deploy scrumify-v1.5.43'
            sh 'kubectl apply -f kubernetes/deployment.yaml'
            sh 'kubectl rollout status deployment/scrumify-v1.5.43 -n=dev-cluster-ze'
          }
        }
        stage('Apply Service Condition') {
          steps {
            sh 'kubectl apply -f kubernetes/service.yaml'
          }
        }
        stage('Apply Istio Configuration') {
          steps {
            sh 'kubectl apply -f kubernetes/istio-config.yaml'
          }
        }
      }
    }
  }

post {
  success {
   echo 'I succeeded!'
 }
 failure {
   slackSend channel: '#scrumify', 
   color: '#ff0000', 
   message: "Build FAILED for ${currentBuild.fullDisplayName} (<${env.RUN_DISPLAY_URL}|view>)."
 }
}
}
