## Scrumify

<details><summary>To ignore</summary>
**NAMESPACE, ISTIO not used, however cluster terraform already do config**  
**MAVEN_GROUPID; MAVEN_ARTIFACTID; MAVEN_VERSION ;CONTAINER_NAME ;CONTAINER_PORT not being used**.  
//MAVEN_GROUPID = readMavenPom().getGroupId()
//MAVEN_ARTIFACTID = readMavenPom().getArtifactId()
//MAVEN_VERSION = readMavenPom().getVersion()
//CONTAINER_NAME = "scrumify-cloud"
//CONTAINER_PORT = "10103"
</details>



At this point we have our GKE cluster up and running along with our database, so we only need to deploy our application scrumify.  
The major steps we want to perform are: 
1.     Compile the application code
2.     Package the application
3.     Process SonarQube on the code
4.     Create a docker image from the Dockerfile (Dockerfile specifies how your code gets packaged into a container)
5.     Push the docker image to a image registry
6.     Create deployment kubernetes object containing the scrumify application
7.     Create the respective kubernetes service
8.     Remove the docker image from the image registry


Assuming that we have Jenkins and SonarQube VM machine running, we will create an CI/CD pipeline by including all these instruction on a Jenkins file.  
The real advantage we got from using a pipeline is that, when we improve the code of scrumify and push a new version to the repository, Jenkins will handle with all compilation, quality and deployment stages of scrumify.  
On the other hand, if anything wrong happens during the course of the pipeline, Jenkins will tell us which stage the pipeline failed and respective reason, which is an important information that can be used for debugging purposes. 


### Associate Jenkins to Git repo


Start by cloning this repository to a personal repo of your own and create a branch with the following format: `<profile>-<cluster_name>`, whether profile can take the values "dev" or "qa".

On the Jenkins virtual machine, open the blue ocean interface and create a new pipeline, select the repository type (i.e. git) and insert your repository url. After that you need to provide a credential to that repository (username and password). This process will create a multibranch pipeline connected to the given repository. Now this multibranch pipeline can run the Jenkinsfile of any branch independently from the other branches (i.e. a push in one branch will only trigger the execution of the pipeline associated with that branch).

In order to run the Jenkinsfile pipeline in a specific branch on every push to that branch, you need to setup a webhook in your repository that will send a notification to the Jenkins machine on every push. In gitlab, that is done in Settings -> Integrations -> Jenkins CI. In this page you can specify when you want the repository to send the notification and where to send it (your Jenkins machine information).


### Jenkins Pipeline



Open Jenkinsfile and fill the fields `environment` and `stages`.
Note: Continue to use the readmes from the original repo.

#### Environment

Before starting building the pipeline, we have to create some variables. These variables value either will be used here on the pipeline context or on the application properties/pom.xml of the application.



`APP_VERSION`
The application version on pom.xml.
This value can be obtain issuing: `mvn help:evaluate -Dexpression=project.version -q -DforceStdout`

`MAVEN_PROFILE`
This is the profile to use when running or packing scrumify.
For now only consider the following the profiles that you can find on pom.xml: "dev" and "qa".
You don't need to hardcode this field. You can retrieve its value by capturing the first word of the branch name, using the "$GIT_BRANCH" variable.  
e.g.: getting the 'qa' out of branch name "qa-mycluster".

`CLUSTER_NAME`
This is the GCP cluster name.
You can retrieve its value by capturing the second word or token of the branch name.

`DEV_DB_URL`; 
`DEV_DB_USER`; 
`DEV_DB_PASS`; 
`QA_DB_URL`; 
`QA_DB_USER`; 
`QA_DB_PASS`

These variables correspond to database urls, users and passwords and they will be inputted in the pom.xml of the application. Since they correspond to sensitive information, you can use jenkins credentials to store this type of information. Thus, we prevent it from being hardcoded and exposed in this project taking the chance of somebody not intended to get it.


Steps to create a new credential on Jenkins server:
1. Go to __Manage Jenkins__
2. Click on __Manage Credentials__
3. Go to __Jenkins store__
4. Click on __Global credentials (unrestricted)__
5. Click on __Add Credentials__

To define the 6 credentials the following values can be used (all of type "Secret Text"):

* ID: `dev-db-url`, Value: `jdbc:postgresql://postgres.${env.BRANCH_NAME}.svc.cluster.local:5432/postgresdb?currentSchema=${spring.database.schema}`, Description: `Database URL for dev profile`
* ID: `dev-db-user`, Value: `postgresadmin`, Description: `User of database in dev profile`
* ID: `dev-db-pass`, Value: `*****`, Description: `Password of database in dev profile`
* ID: `qa-db-url`, Value: `jdbc:postgresql://postgres.${env.BRANCH_NAME}.svc.cluster.local:5432/postgresdb?currentSchema=${spring.database.schema}`, Description: `Database URL for qa profile`
* ID: `qa-db-user`, Value: `postgresadmin`, Description: `User of database in qa profile`
* ID: `qa-db-pass`, Value: `*****`, Description: `Password of database in qa profile`


For the case of `DEV_DB_URL`, after you created the credential 'dev-db-url' in Jenkins, here on the pipeline context you can set this variable by doing: 
```sh
DEV_DB_URL = credentials('dev-db-url')
```

#### Stages

Create the following stages using the name provided and respecting the instructions for each stage:

##### Stage: Build

Add these 2 commands to your list of steps.

- Compilation: `mvn compile -DskipTests`
- Packaging: `mvn clean package -P${MAVEN_PROFILE} -DskipTests -Dmaven.install.skip=true`

The end result of this stage, should look like this:
```sh
stage('Build') {
    steps{
        sh'<command #1>'
        sh'<command #2>'
    }
}
```

##### Stage: SonarQube

You can add this snippet into your Jenkinsfile.  
It evaluates the application in terms of test coverage, duplicated lines, maintainability, reliability and security ratings and security hotsposts.

```sh
stage('SonarQube') {
    stages {
        stage('SonarQube analysis'){
        steps {
            withSonarQubeEnv('sonarqube'){
            sh "mvn sonar:sonar"
            }
        }
    }
    stage("SonarQube Quality Gate"){
    steps {
        script {
        timeout(time: 5, unit: 'MINUTES') {
            def qg = waitForQualityGate()
            if (qg.status != 'OK') {
            error "Pipeline aborted due to quality gate failure: ${qg.status}"
            }
        }
        }
    }
    }
    }
}
```

##### Stage: Docker Build

-  Register gcloud as a Docker credential helper: `gcloud auth configure-docker`  
-  Docker build:
    ```sh
    Tag: --tag=scrumify-${BRANCH_NAME}:v${APP_VERSION}
    Path: ./
    ```
    Docker build documentation: 
    https://docs.docker.com/engine/reference/commandline/build/



##### Stage: Publish Image

- Tag docker image
    ```sh
    source: scrumify-${BRANCH_NAME}:v${APP_VERSION}     
    target: eu.gcr.io/${GCP_PROJECT_ID}/scrumify-${BRANCH_NAME}:v${APP_VERSION}   
    ```
    Docker image tag documentation: https://docs.docker.com/engine/reference/commandline/image_tag/

- Push image to registry:
`docker push eu.gcr.io/${GCP_PROJECT_ID}/scrumify-${BRANCH_NAME}:v${APP_VERSION}`

##### Stage: Deployment

Before start doing modifications into the cluster, this is issuing ctl commands, it's a good practice to make sure which cluster are we talking to.  
Place this command on the beginning of this stage: `gcloud container clusters get-credentials ${CLUSTER_NAME} --region <project_zone> --project ${GCP_PROJECT_ID}`

Brief explanation about the files involved in this stage:

1. deployment.yaml
In this file, you will define the application which will be deployed in the Kubernetes
also in this file, you canto define how to verify if the application is up (livenessProbe), ready(readinessProbe) and also some annotations as to how Prometheus will be able to scrape the application and any other configuration that the application will need, including the selector which will be necessary by the service.  
    documentation: https://kubernetes.io/docs/concepts/workloads/controllers/deployment/

2. service.yaml 
This file will define the access point for the deployment based on the selector.  
obs: since we are using istio as ingress point we are setting which is the version selector in the DestinationRule label.  
    documentation: https://kubernetes.io/docs/concepts/services-networking/service/

3. istio-config.yaml 
This will define the configuration about istio and how it will connect the ingress gateway with your application. Basically in this file, we have defined the Gateway, VirtualService, DestinationRule.  
    documentation: https://istio.io/latest/docs/setup/getting-started/

    3.1 Gateway
    It will basically define what will be at the ingress point of the mesh, protocols, hosts needs to be configured here.  
        documentation: https://istio.io/latest/docs/reference/config/networking/gateway

    3.2 VirtualService
    Here it´s possible to configure the traffic routing, it can be based on headers, hosts, it´s also possible to create weighting configurations, you also need to configure here which will be the subset to be used in the destination rule.  
        documentation: https://istio.io/latest/docs/reference/config/networking/virtual-service/

    3.3 DestinationRule
    Here you can define policies to apply to the traffic, define subsets to segregate the versions.  
        documentation: https://istio.io/latest/docs/reference/config/networking/destination-rule/

```sh
1. File kubernetes/namespace.yaml
    Replace "##Branch##" by ${BRANCH_NAME}
2. File: kubernetes/deployment.yaml
    Replace "##Project##" by ${GCP_PROJECT_ID}
            "##Revision##" by ${APP_VERSION}
            "##Branch##" by ${BRANCH_NAME}
3. File: kubernetes/service.yaml
    Replace "##Revision##" by ${APP_VERSION}
            "##Branch##" by ${BRANCH_NAME}
4. File: kubernetes/istio-config.yaml
    Replace "##Revision##" by ${APP_VERSION}
            "##Branch##" by ${BRANCH_NAME}
    

5. Apply the kubernetes/namespace.yaml, only on the first jenkins execution
6. Apply kubernetes/deployment.yaml and after that consider using kubectl rollout status command for Jenkins to wait until the deployment is performed
7. Apply kubernetes/service.yaml, only on the first Jenkins execution
8. Apply kubernetes/istio-config.yaml

9. Delete the previous kubernetes deployment, after the first jenkins execution.
10. Remove docker image from docker
11. Remove docker image from the registry

```

### Execute Jenkins Pipeline & Interact with Application

After creating the whole Jenkins pipeline, you can trigger it to place on top of the cluster a scrumify application with the version previously specified.
You can watch on Jenkins > BlueOcean the execution of your pipeline. If the pipeline had succeded, try to interact with the scrumify using a web browser.

    1. Trigger Jenkins pipeline
    2. Interact with the scurmify application

